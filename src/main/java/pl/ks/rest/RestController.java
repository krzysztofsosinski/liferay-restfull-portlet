/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pl.ks.rest;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.CompanyConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.Authenticator;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;

@Controller
@RequestMapping("/api")
public class RestController {

    private static final Log LOGGER = LogFactoryUtil.getLog(RestController.class);
    private static final String SECRET = "dj4DORYFKU";
    private static final long ONE_MINUTE_IN_MILLIS = 60000;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> test(HttpServletRequest request){
        return ResponseEntity.ok("It works !!");
    }

    @RequestMapping(value = "/auth/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> login(HttpServletRequest request, @RequestBody UserLogin userLogin) throws SystemException, PortalException, UnsupportedEncodingException {
        LOGGER.info("login: " + userLogin.getLogin());
        LOGGER.info("password: " + userLogin.getPassword());

        Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
        long status = UserLocalServiceUtil.authenticateForBasic(company.getCompanyId(), CompanyConstants.AUTH_TYPE_EA, userLogin.getLogin(), userLogin.getPassword());

        LOGGER.info("status: " + status);

        User user = null;
        if(status >= Authenticator.SUCCESS){
            user = UserLocalServiceUtil.fetchUser(status);
        }

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Date now = new Date();

        String jwt = Jwts.builder().setSubject(user.getScreenName())
                .claim("name", user.getFullName())
                .claim("group", "reseller")
                .setIssuedAt(now).setExpiration(new Date(now.getTime() + (5 * ONE_MINUTE_IN_MILLIS)))
                .signWith(SignatureAlgorithm.HS256, SECRET.getBytes("UTF-8")).compact();

        return ResponseEntity.ok(new TokenResponse(jwt, user.getEmailAddress()));
    }

    @RequestMapping(value = "/user/{id}")
    @ResponseBody
    public ResponseEntity<?> getUserById(@PathVariable Long id, HttpServletRequest httpServletRequest) throws UnsupportedEncodingException, SystemException {
        String auth = httpServletRequest.getHeader("Authorization");
        if(auth == null || !auth.contains("Bearer")){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        String jwt = auth.split("\\s")[1];
        LOGGER.info("jwt: " + jwt);
        Jws<Claims> claims = Jwts.parser()
                .setSigningKey(SECRET.getBytes("UTF-8"))
                .parseClaimsJws(jwt);
        String group = (String) claims.getBody().get("group");
        if(!group.equals("reseller")){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        LOGGER.info("Looking for: " + (id != null));
        User user = UserLocalServiceUtil.fetchUser(id);
        if(user != null) {
            LOGGER.info("user found: " + user.getFullName());
            return ResponseEntity.ok(new UserDTO(user.getUserId(), user.getFullName(), user.getEmailAddress()));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}