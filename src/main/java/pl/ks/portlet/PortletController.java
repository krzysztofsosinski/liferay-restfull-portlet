package pl.ks.portlet;

import com.liferay.portal.kernel.util.ReleaseInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

@Controller
@RequestMapping("VIEW")
public class PortletController {

    @RenderMapping
    public String index(RenderRequest request, RenderResponse response, Model model) {
        model.addAttribute("releaseInfo", ReleaseInfo.getReleaseInfo());
        return "rest-portlet/view";
    }

}
