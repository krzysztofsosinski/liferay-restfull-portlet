<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>

<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.liferay.portlet.PortletPreferencesFactoryUtil" %>
<%@ page import="javax.portlet.PortletPreferences"%>
<%@ page import="java.util.Locale"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

This is the <b>rest-portlet</b> portlet.<br />

<form method="POST" id="login-form" action="${themeDisplay.portalURL}/delegate/api/auth/login">
    <h1>Login Form</h1>
    <label>Login:
        <input type="text" id="login" name="login"/>
    </label><br>
    <label>Password:
        <input type="password" id="password" name="password"/>
    </label>
    <input type="submit" id="submit-btn" name="submit">
</form>

<label>User id: <input type="text" name="userId" id="userId"></label>
<button name="send" title="send" id="searchUser">Search user</button>

<pre class="result-json">

</pre>

<c:out escapeXml="true" value="${releaseInfo}" />.

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        var jwt = {};

        $('#submit-btn').on('click', function (e) {
            var url = $("#login-form").attr('action');

            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                type: "POST",
                url: url,
                dataType: 'json',
                data: JSON.stringify({
                    login: $("#login").val(),
                    password: $('#password').val()
                }),
                success: function(data) {
                    jwt = data;
                }
            });

            e.preventDefault();

        });

        $('#searchUser').on('click', function(e){
           if(jwt.hasOwnProperty("token")){
               $.ajax({
                   headers: {
                       'Authorization': 'Bearer '+ jwt.token
                   },
                   type: 'GET',
                   url: '${themeDisplay.portalURL}/delegate/api/user/' + $('#userId').val(),
                   success: function(data) {
                        $('.result-json').text(JSON.stringify(data));
                   }
               })
           }
        });

    });
</script>